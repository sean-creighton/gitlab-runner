package common

import (
	"fmt"
	"sync"

	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/gitlab-runner/helpers"
	"gitlab.com/gitlab-org/gitlab-runner/helpers/url"
)

type BuildLogger struct {
	log   JobTrace
	entry *logrus.Entry
	mutex sync.Mutex
}

func (e *BuildLogger) WithFields(fields logrus.Fields) BuildLogger {
	return NewBuildLogger(e.log, e.entry.WithFields(fields))
}

func (e *BuildLogger) SendRawLog(args ...interface{}) {
	if e.log != nil {
		fmt.Fprint(e.log, args...)
	}
}

func (e *BuildLogger) sendLog(logger func(args ...interface{}), logPrefix string, args ...interface{}) {
	if e.log != nil {
		logLine := url_helpers.ScrubSecrets(logPrefix + fmt.Sprintln(args...))
		e.SendRawLog(logLine)
		e.SendRawLog(helpers.ANSI_RESET)

		if e.log.IsStdout() {
			return
		}
	}

	if len(args) == 0 {
		return
	}

	logger(args...)
}

func (e *BuildLogger) Debugln(args ...interface{}) {
	if e.entry == nil {
		return
	}
	e.mutex.Lock()
	e.entry.Debugln(args...)
	e.mutex.Unlock()
}

func (e *BuildLogger) Println(args ...interface{}) {
	if e.entry == nil {
		return
	}
	e.mutex.Lock()
	e.sendLog(e.entry.Debugln, helpers.ANSI_CLEAR, args...)
	e.mutex.Unlock()
}

func (e *BuildLogger) Infoln(args ...interface{}) {
	if e.entry == nil {
		return
	}
	e.mutex.Lock()
	e.sendLog(e.entry.Println, helpers.ANSI_BOLD_GREEN, args...)
	e.mutex.Unlock()
}

func (e *BuildLogger) Warningln(args ...interface{}) {
	if e.entry == nil {
		return
	}
	e.mutex.Lock()
	e.sendLog(e.entry.Warningln, helpers.ANSI_YELLOW+"WARNING: ", args...)
	e.mutex.Unlock()
}

func (e *BuildLogger) SoftErrorln(args ...interface{}) {
	if e.entry == nil {
		return
	}
	e.mutex.Lock()
	e.sendLog(e.entry.Warningln, helpers.ANSI_BOLD_RED+"ERROR: ", args...)
	e.mutex.Unlock()
}

func (e *BuildLogger) Errorln(args ...interface{}) {
	if e.entry == nil {
		return
	}
	e.mutex.Lock()
	e.sendLog(e.entry.Errorln, helpers.ANSI_BOLD_RED+"ERROR: ", args...)
	e.mutex.Unlock()
}

func NewBuildLogger(log JobTrace, entry *logrus.Entry) BuildLogger {
	return BuildLogger{
		log:   log,
		entry: entry,
	}
}
